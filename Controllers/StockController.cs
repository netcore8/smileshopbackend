using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmileShopByKhunmiw.DTOs.Stock;
using SmileShopByKhunmiw.Services.Stock;

namespace SmileShopByKhunmiw.Controllers
{

    [ApiController]
    [Route("api/[controller]")]

    public class StockController : ControllerBase
    {
        private readonly IStockService _stockService;
        public StockController(IStockService stockService)
        {
            this._stockService = stockService;

        }

        [HttpPost("addstock")]
        public async Task<IActionResult> Add(AddStockDto newStock)
        {
            return Ok(await _stockService.Add(newStock));
        }

        [HttpGet("filter")]
        public async Task<IActionResult> filter([FromQuery] StockFilterDto filter)
        {
            return Ok(await _stockService.Filter(filter));
        }


    }
}