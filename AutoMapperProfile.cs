﻿using AutoMapper;
using SmileShopByKhunmiw.DTOs;
using SmileShopByKhunmiw.DTOs.Person;
using SmileShopByKhunmiw.DTOs.Product;
using SmileShopByKhunmiw.DTOs.Stock;
using SmileShopByKhunmiw.Models;
using SmileShopByKhunmiw.Models.Auth;
using SmileShopByKhunmiw.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmileShopByKhunmiw
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<Role, RoleDto>()
                .ForMember(x => x.RoleName, x => x.MapFrom(x => x.Name));
            CreateMap<RoleDtoAdd, Role>()
                .ForMember(x => x.Name, x => x.MapFrom(x => x.RoleName)); ;
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<Person, PersonDto>();
            CreateMap<ProductGroup, GetProductGroupDto>();
            CreateMap<Product, GetProductDto>();
            CreateMap<Stock, GetStockDto>();

            // .ForMember(x => x.Product, x => x.MapFrom(x => x.Product.ProductGroup));
        }
    }
}