namespace SmileShopByKhunmiw.DTOs.Product
{
    public class UpdateProductGroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int StatusId { get; set; }
    }
}