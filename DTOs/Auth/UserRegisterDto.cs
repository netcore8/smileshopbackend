﻿using System.ComponentModel.DataAnnotations;
using SmileShopByKhunmiw.DTOs.Person;

namespace SmileShopByKhunmiw.DTOs
{
    public class UserRegisterDto : AddPersonDto
    {
        [Required]
        [StringLength(20)]
        public string Username { get; set; }

        [Required]
        [StringLength(20)]
        public string Password { get; set; }

    }
}