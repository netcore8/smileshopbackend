﻿using SmileShopByKhunmiw.Validations;

namespace SmileShopByKhunmiw.DTOs
{
    public class RoleDtoAdd
    {
        [FirstLetterUpperCase]
        public string RoleName { get; set; }
    }
}