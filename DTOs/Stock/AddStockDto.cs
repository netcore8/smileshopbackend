namespace SmileShopByKhunmiw.DTOs.Stock
{
    public class AddStockDto
    {
        public int ProductId { get; set; }

        public int StockTypeId { get; set; }

        public int AmountBefore { get; set; }
        public int AmountStock { get; set; }
        public int AmountAfter { get; set; }
        public string Remark { get; set; }
    }
}