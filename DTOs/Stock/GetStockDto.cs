using System;
using SmileShopByKhunmiw.DTOs.Product;

namespace SmileShopByKhunmiw.DTOs.Stock
{
    public class GetStockDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        //public string ProductName { get; set; }

        public GetProductDto Product { get; set; }

        public int StockTypeId { get; set; }

        public int AmountBefore { get; set; }
        public int AmountStock { get; set; }
        public int AmountAfter { get; set; }
        public string Remark { get; set; }
        public Guid CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }



    }
}