namespace SmileShopByKhunmiw.DTOs.Stock
{
    public class StockFilterDto : PaginationDto
    {
        public int ProductGroupId { get; set; }
        public int ProductId { get; set; }
        public string OrderingField { get; set; }

        public bool AscendingOrder { get; set; } = true;

    }
}