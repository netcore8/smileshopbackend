﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class PersonCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("531e713a-f058-4797-94d5-f16285f30502"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("57e0bf9f-4625-4e39-a35a-a29d6bf7c910"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("62bfb6f0-98b7-43ec-a1ec-0db49dbbad03"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f5d39aee-3f36-4436-9aba-ec42628928f1"));

            migrationBuilder.EnsureSchema(
                name: "person");

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                schema: "auth",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Person",
                schema: "person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("575185cf-8263-43b7-ba82-55e2c4f9c047"), "User" },
                    { new Guid("04f15af5-8ec4-47d7-ab0d-7245bd18ec6d"), "Manager" },
                    { new Guid("794f8c80-22f6-4e0d-b735-6ec4ea09abef"), "Admin" },
                    { new Guid("bd92f796-27b8-4827-968c-9b4cb37f5ab5"), "Developer" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_PersonId",
                schema: "auth",
                table: "User",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_User_Person_PersonId",
                schema: "auth",
                table: "User",
                column: "PersonId",
                principalSchema: "person",
                principalTable: "Person",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_Person_PersonId",
                schema: "auth",
                table: "User");

            migrationBuilder.DropTable(
                name: "Person",
                schema: "person");

            migrationBuilder.DropIndex(
                name: "IX_User_PersonId",
                schema: "auth",
                table: "User");

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("04f15af5-8ec4-47d7-ab0d-7245bd18ec6d"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("575185cf-8263-43b7-ba82-55e2c4f9c047"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("794f8c80-22f6-4e0d-b735-6ec4ea09abef"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("bd92f796-27b8-4827-968c-9b4cb37f5ab5"));

            migrationBuilder.DropColumn(
                name: "PersonId",
                schema: "auth",
                table: "User");

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("57e0bf9f-4625-4e39-a35a-a29d6bf7c910"), "user" },
                    { new Guid("f5d39aee-3f36-4436-9aba-ec42628928f1"), "Manager" },
                    { new Guid("531e713a-f058-4797-94d5-f16285f30502"), "Admin" },
                    { new Guid("62bfb6f0-98b7-43ec-a1ec-0db49dbbad03"), "Developer" }
                });
        }
    }
}
