﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class addstatusIdCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("03352dd2-382d-45fc-bc0d-d99148290e76"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("0b9fac8a-65ff-4bad-8c2d-589733675b0e"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("67b31c46-63a1-4ffe-b0b6-6d62d6d266e0"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7d3aec0d-ca6c-4e97-b769-35e42ed6180d"));

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                schema: "product",
                table: "ProductGroup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("6fb6d83b-31e1-44de-b857-23f43635b9d3"), "User" },
                    { new Guid("7b5481c9-13b5-47c4-af1f-122b1e4cf23f"), "Manager" },
                    { new Guid("80634639-e638-4646-b851-2934b488043f"), "Admin" },
                    { new Guid("7c27b676-a32e-4556-9673-960cb7561405"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("6fb6d83b-31e1-44de-b857-23f43635b9d3"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7b5481c9-13b5-47c4-af1f-122b1e4cf23f"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7c27b676-a32e-4556-9673-960cb7561405"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("80634639-e638-4646-b851-2934b488043f"));

            migrationBuilder.DropColumn(
                name: "StatusId",
                schema: "product",
                table: "ProductGroup");

            migrationBuilder.DropColumn(
                name: "StatusId",
                schema: "product",
                table: "Product");

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("7d3aec0d-ca6c-4e97-b769-35e42ed6180d"), "User" },
                    { new Guid("0b9fac8a-65ff-4bad-8c2d-589733675b0e"), "Manager" },
                    { new Guid("67b31c46-63a1-4ffe-b0b6-6d62d6d266e0"), "Admin" },
                    { new Guid("03352dd2-382d-45fc-bc0d-d99148290e76"), "Developer" }
                });
        }
    }
}
