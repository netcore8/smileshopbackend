﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class ProductCreate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("0d61ee81-6472-44f9-862e-03320ba7254d"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("d1cd9731-a6dc-4a95-b890-fb89f05a09bf"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("e7cef556-e5d1-47be-a1c9-d2bc9d596787"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f65ad06b-2f59-429a-bd38-ee770bf41f23"));

            migrationBuilder.CreateTable(
                name: "Product",
                schema: "product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Stock = table.Column<int>(nullable: false),
                    ProductGroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_ProductGroup_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalSchema: "product",
                        principalTable: "ProductGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("2bbb6f8f-0c49-46e4-ad85-978a12a9e942"), "User" },
                    { new Guid("c6fcc1ea-e204-44a7-93e2-78e3dbd9d8a1"), "Manager" },
                    { new Guid("bbd0c386-1e77-4819-8693-3881be71d873"), "Admin" },
                    { new Guid("950df9af-aa72-4c5d-9b6e-9aa55416b5e1"), "Developer" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProductGroupId",
                schema: "product",
                table: "Product",
                column: "ProductGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product",
                schema: "product");

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("2bbb6f8f-0c49-46e4-ad85-978a12a9e942"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("950df9af-aa72-4c5d-9b6e-9aa55416b5e1"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("bbd0c386-1e77-4819-8693-3881be71d873"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("c6fcc1ea-e204-44a7-93e2-78e3dbd9d8a1"));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("e7cef556-e5d1-47be-a1c9-d2bc9d596787"), "User" },
                    { new Guid("f65ad06b-2f59-429a-bd38-ee770bf41f23"), "Manager" },
                    { new Guid("0d61ee81-6472-44f9-862e-03320ba7254d"), "Admin" },
                    { new Guid("d1cd9731-a6dc-4a95-b890-fb89f05a09bf"), "Developer" }
                });
        }
    }
}
