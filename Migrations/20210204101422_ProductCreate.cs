﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class ProductCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("29270da5-b808-4f56-94ee-22e4370def76"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("46e179c6-0f2e-4e24-8911-0bb401023365"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("73cee0df-fe60-4ff5-ba55-21379e1c3c2b"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("b1597b68-4ddc-4edf-a8ce-5a00f5a8b12f"));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "product",
                table: "ProductGroup",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("e7cef556-e5d1-47be-a1c9-d2bc9d596787"), "User" },
                    { new Guid("f65ad06b-2f59-429a-bd38-ee770bf41f23"), "Manager" },
                    { new Guid("0d61ee81-6472-44f9-862e-03320ba7254d"), "Admin" },
                    { new Guid("d1cd9731-a6dc-4a95-b890-fb89f05a09bf"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("0d61ee81-6472-44f9-862e-03320ba7254d"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("d1cd9731-a6dc-4a95-b890-fb89f05a09bf"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("e7cef556-e5d1-47be-a1c9-d2bc9d596787"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f65ad06b-2f59-429a-bd38-ee770bf41f23"));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "product",
                table: "ProductGroup",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("73cee0df-fe60-4ff5-ba55-21379e1c3c2b"), "User" },
                    { new Guid("29270da5-b808-4f56-94ee-22e4370def76"), "Manager" },
                    { new Guid("b1597b68-4ddc-4edf-a8ce-5a00f5a8b12f"), "Admin" },
                    { new Guid("46e179c6-0f2e-4e24-8911-0bb401023365"), "Developer" }
                });
        }
    }
}
