﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class addIsActiveProductCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("230d022e-4cc2-447e-b02d-718bbd13cc46"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("53bd927d-fbc7-4ba8-aa3e-56aaf364eb3f"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("a4f9d2d7-d49f-4c36-bee5-b26ee0043fe4"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("aac6b6db-18b4-4aab-80b7-3b23645b648e"));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "product",
                table: "ProductGroup",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("3b513b3f-5b48-45eb-a0cb-665822082014"), "User" },
                    { new Guid("caca59dc-7953-4b43-8b56-b7041beada90"), "Manager" },
                    { new Guid("f43e80c7-78f3-4aa3-8164-f3ecc62b4601"), "Admin" },
                    { new Guid("e7822ac9-1d0e-4a57-9cca-0b5d922fca6e"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("3b513b3f-5b48-45eb-a0cb-665822082014"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("caca59dc-7953-4b43-8b56-b7041beada90"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("e7822ac9-1d0e-4a57-9cca-0b5d922fca6e"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f43e80c7-78f3-4aa3-8164-f3ecc62b4601"));

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "product",
                table: "ProductGroup");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "product",
                table: "Product");

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("a4f9d2d7-d49f-4c36-bee5-b26ee0043fe4"), "User" },
                    { new Guid("230d022e-4cc2-447e-b02d-718bbd13cc46"), "Manager" },
                    { new Guid("aac6b6db-18b4-4aab-80b7-3b23645b648e"), "Admin" },
                    { new Guid("53bd927d-fbc7-4ba8-aa3e-56aaf364eb3f"), "Developer" }
                });
        }
    }
}
