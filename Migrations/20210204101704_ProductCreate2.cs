﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class ProductCreate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("2bbb6f8f-0c49-46e4-ad85-978a12a9e942"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("950df9af-aa72-4c5d-9b6e-9aa55416b5e1"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("bbd0c386-1e77-4819-8693-3881be71d873"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("c6fcc1ea-e204-44a7-93e2-78e3dbd9d8a1"));

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("a4f9d2d7-d49f-4c36-bee5-b26ee0043fe4"), "User" },
                    { new Guid("230d022e-4cc2-447e-b02d-718bbd13cc46"), "Manager" },
                    { new Guid("aac6b6db-18b4-4aab-80b7-3b23645b648e"), "Admin" },
                    { new Guid("53bd927d-fbc7-4ba8-aa3e-56aaf364eb3f"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("230d022e-4cc2-447e-b02d-718bbd13cc46"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("53bd927d-fbc7-4ba8-aa3e-56aaf364eb3f"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("a4f9d2d7-d49f-4c36-bee5-b26ee0043fe4"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("aac6b6db-18b4-4aab-80b7-3b23645b648e"));

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "product",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                schema: "product",
                table: "Product");

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("2bbb6f8f-0c49-46e4-ad85-978a12a9e942"), "User" },
                    { new Guid("c6fcc1ea-e204-44a7-93e2-78e3dbd9d8a1"), "Manager" },
                    { new Guid("bbd0c386-1e77-4819-8693-3881be71d873"), "Admin" },
                    { new Guid("950df9af-aa72-4c5d-9b6e-9aa55416b5e1"), "Developer" }
                });
        }
    }
}
