﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class stockCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("6fb6d83b-31e1-44de-b857-23f43635b9d3"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7b5481c9-13b5-47c4-af1f-122b1e4cf23f"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7c27b676-a32e-4556-9673-960cb7561405"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("80634639-e638-4646-b851-2934b488043f"));

            migrationBuilder.CreateTable(
                name: "Stock",
                schema: "product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(nullable: false),
                    StockTypeId = table.Column<int>(nullable: false),
                    AmountBefore = table.Column<int>(nullable: false),
                    AmountStock = table.Column<int>(nullable: false),
                    AmountAfter = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("2c1ce9bb-be0a-44c6-88a1-43d4a73d48c4"), "User" },
                    { new Guid("34a1779e-b4dd-47a8-9d4a-c28cc616a998"), "Manager" },
                    { new Guid("49321fdc-d2bb-4b64-9ed1-1087aa70b5eb"), "Admin" },
                    { new Guid("91e75b94-522c-4d2e-8736-91285d57f0aa"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Stock",
                schema: "product");

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("2c1ce9bb-be0a-44c6-88a1-43d4a73d48c4"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("34a1779e-b4dd-47a8-9d4a-c28cc616a998"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("49321fdc-d2bb-4b64-9ed1-1087aa70b5eb"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("91e75b94-522c-4d2e-8736-91285d57f0aa"));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("6fb6d83b-31e1-44de-b857-23f43635b9d3"), "User" },
                    { new Guid("7b5481c9-13b5-47c4-af1f-122b1e4cf23f"), "Manager" },
                    { new Guid("80634639-e638-4646-b851-2934b488043f"), "Admin" },
                    { new Guid("7c27b676-a32e-4556-9673-960cb7561405"), "Developer" }
                });
        }
    }
}
