﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class addUpdateByCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("3b513b3f-5b48-45eb-a0cb-665822082014"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("caca59dc-7953-4b43-8b56-b7041beada90"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("e7822ac9-1d0e-4a57-9cca-0b5d922fca6e"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f43e80c7-78f3-4aa3-8164-f3ecc62b4601"));

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedBy",
                schema: "product",
                table: "ProductGroup",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                schema: "product",
                table: "ProductGroup",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedBy",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                schema: "product",
                table: "Product",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("7d3aec0d-ca6c-4e97-b769-35e42ed6180d"), "User" },
                    { new Guid("0b9fac8a-65ff-4bad-8c2d-589733675b0e"), "Manager" },
                    { new Guid("67b31c46-63a1-4ffe-b0b6-6d62d6d266e0"), "Admin" },
                    { new Guid("03352dd2-382d-45fc-bc0d-d99148290e76"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("03352dd2-382d-45fc-bc0d-d99148290e76"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("0b9fac8a-65ff-4bad-8c2d-589733675b0e"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("67b31c46-63a1-4ffe-b0b6-6d62d6d266e0"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("7d3aec0d-ca6c-4e97-b769-35e42ed6180d"));

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                schema: "product",
                table: "ProductGroup");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                schema: "product",
                table: "ProductGroup");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                schema: "product",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                schema: "product",
                table: "Product");

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("3b513b3f-5b48-45eb-a0cb-665822082014"), "User" },
                    { new Guid("caca59dc-7953-4b43-8b56-b7041beada90"), "Manager" },
                    { new Guid("f43e80c7-78f3-4aa3-8164-f3ecc62b4601"), "Admin" },
                    { new Guid("e7822ac9-1d0e-4a57-9cca-0b5d922fca6e"), "Developer" }
                });
        }
    }
}
