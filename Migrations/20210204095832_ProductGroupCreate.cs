﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmileShopByKhunmiw.Migrations
{
    public partial class ProductGroupCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("04f15af5-8ec4-47d7-ab0d-7245bd18ec6d"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("575185cf-8263-43b7-ba82-55e2c4f9c047"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("794f8c80-22f6-4e0d-b735-6ec4ea09abef"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("bd92f796-27b8-4827-968c-9b4cb37f5ab5"));

            migrationBuilder.EnsureSchema(
                name: "product");

            migrationBuilder.CreateTable(
                name: "ProductGroup",
                schema: "product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroup", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("73cee0df-fe60-4ff5-ba55-21379e1c3c2b"), "User" },
                    { new Guid("29270da5-b808-4f56-94ee-22e4370def76"), "Manager" },
                    { new Guid("b1597b68-4ddc-4edf-a8ce-5a00f5a8b12f"), "Admin" },
                    { new Guid("46e179c6-0f2e-4e24-8911-0bb401023365"), "Developer" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductGroup",
                schema: "product");

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("29270da5-b808-4f56-94ee-22e4370def76"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("46e179c6-0f2e-4e24-8911-0bb401023365"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("73cee0df-fe60-4ff5-ba55-21379e1c3c2b"));

            migrationBuilder.DeleteData(
                schema: "auth",
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("b1597b68-4ddc-4edf-a8ce-5a00f5a8b12f"));

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Role",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("575185cf-8263-43b7-ba82-55e2c4f9c047"), "User" },
                    { new Guid("04f15af5-8ec4-47d7-ab0d-7245bd18ec6d"), "Manager" },
                    { new Guid("794f8c80-22f6-4e0d-b735-6ec4ea09abef"), "Admin" },
                    { new Guid("bd92f796-27b8-4827-968c-9b4cb37f5ab5"), "Developer" }
                });
        }
    }
}
