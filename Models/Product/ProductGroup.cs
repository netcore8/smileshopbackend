using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmileShopByKhunmiw.Models.Base;

namespace SmileShopByKhunmiw.Models.Product
{

    [Table("ProductGroup", Schema = "product")]
    public class ProductGroup : Update
    {

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public List<Product> Product { get; set; }

        public int StatusId { get; set; }

    }
}