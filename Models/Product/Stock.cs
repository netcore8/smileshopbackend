using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmileShopByKhunmiw.Models.Product
{
    [Table("Stock", Schema = "product")]

    public class Stock
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int StockTypeId { get; set; }

        public int AmountBefore { get; set; }
        public int AmountStock { get; set; }
        public int AmountAfter { get; set; }
        public string Remark { get; set; }
        public Guid CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }



    }
}