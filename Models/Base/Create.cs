using System;

namespace SmileShopByKhunmiw.Models.Base
{
    public class Create
    {
        public Guid CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}