using System;

namespace SmileShopByKhunmiw.Models.Base
{
    public class Update
    {
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}