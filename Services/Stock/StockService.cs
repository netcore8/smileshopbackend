using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmileShopByKhunmiw.Data;
using SmileShopByKhunmiw.DTOs.Stock;
using SmileShopByKhunmiw.Models;
using System.Linq.Dynamic.Core;
using SmileShopByKhunmiw.Helpers;

namespace SmileShopByKhunmiw.Services.Stock
{
    public class StockService : ServiceBase, IStockService
    {
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDBContext _dBContext;
        private readonly ILogger<StockService> _log;

        public StockService(IMapper mapper, IHttpContextAccessor httpContext, AppDBContext dBContext, ILogger<StockService> log) : base(dBContext, mapper, httpContext)
        {
            this._mapper = mapper;
            this._httpContext = httpContext;
            this._dBContext = dBContext;
            this._log = log;
        }
        public async Task<ServiceResponse<GetStockDto>> Add(AddStockDto newStock)
        {
            var product = await _dBContext.Products.Where(x => x.Id == newStock.ProductId).SingleOrDefaultAsync();

            if (product is null)
            {
                _log.LogError("Error: Product not found");
                return ResponseResult.Failure<GetStockDto>($"Product not found ProductID : {newStock.ProductId}");
            }

            //Update product 
            product.Stock = newStock.AmountAfter;
            product.UpdatedBy = Guid.Parse(GetUserId());
            product.UpdatedDate = Now();

            _dBContext.Products.Update(product);
            await _dBContext.SaveChangesAsync();


            var stock = new Models.Product.Stock();
            stock.ProductId = newStock.ProductId;
            stock.AmountBefore = newStock.AmountBefore;
            stock.AmountAfter = newStock.AmountAfter;
            stock.AmountStock = newStock.AmountStock;
            stock.StockTypeId = newStock.StockTypeId;
            stock.Remark = newStock.Remark;
            stock.CreatedById = Guid.Parse(GetUserId());
            stock.CreatedDate = Now();

            await _dBContext.Stocks.AddAsync(stock);
            await _dBContext.SaveChangesAsync();

            var dto = _mapper.Map<Models.Product.Stock, GetStockDto>(stock);


            return ResponseResult.Success(dto);
        }

        public async Task<ServiceResponseWithPagination<List<GetStockDto>>> Filter(StockFilterDto Filter)
        {
            //Add data to queryable
            var queryable = _dBContext.Stocks
            .Include(x => x.Product)
            .ThenInclude(x => x.ProductGroup)
            .AsQueryable();


            if (Filter.ProductGroupId != 0)
            {
                queryable = queryable.Where(x => x.Product.ProductGroupId == Filter.ProductGroupId);
            }

            if (Filter.ProductId != 0)
            {
                queryable = queryable.Where(x => x.ProductId == Filter.ProductId);
            }

            if (!string.IsNullOrWhiteSpace(Filter.OrderingField))
            {
                try
                {
                    queryable = queryable.OrderBy($"{Filter.OrderingField} {(Filter.AscendingOrder ? "ascending" : "descending")}");
                }
                catch
                {
                    return ResponseResultWithPagination.Failure<List<GetStockDto>>($"Could not Stock by field: {Filter.OrderingField}");
                }
            }

            //add data to pagination
            var paginationResult = await _httpContext.HttpContext
           .InsertPaginationParametersInResponse(queryable, Filter.RecordsPerPage, Filter.Page);

            var lst_Stock = await queryable.Paginate(Filter).ToListAsync();

            var dto = _mapper.Map<List<GetStockDto>>(lst_Stock);

            return ResponseResultWithPagination.Success(dto, paginationResult);


        }
    }
}