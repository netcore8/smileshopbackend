using System.Collections.Generic;
using System.Threading.Tasks;
using SmileShopByKhunmiw.DTOs.Stock;
using SmileShopByKhunmiw.Models;

namespace SmileShopByKhunmiw.Services.Stock
{
    public interface IStockService
    {
        Task<ServiceResponse<GetStockDto>> Add(AddStockDto newStock);

        Task<ServiceResponseWithPagination<List<GetStockDto>>> Filter(StockFilterDto Filter);
    }
}